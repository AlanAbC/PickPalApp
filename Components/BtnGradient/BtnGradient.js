import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground} from 'react-native'; 
class BtnGradient extends Component {
    state = {
        color: this.props.borderColor,
        selected: false
    };
    render = () => {
        return(
            <TouchableOpacity onPress = {this.props.onPress} style={Style(this.props.marginTop, this.props.width)}>
                <ImageBackground source = {require('../../Img/gradient.png')} resizeMode= {'cover'} style = {{width: '100%', height: '100%', flexDirection: 'row', alignItems: 'center',}} resizeMethod={'resize'} >
                    <Text style = {{color: 'white', fontSize: 20, fontWeight: 'bold', width: '100%', textAlign: 'center'}}>{this.props.text}</Text>
                </ImageBackground>
                
            </TouchableOpacity>
        );
    }
}
const Style = function(marginTop, width){
    return {
        width: width,
        height: 50,
        marginTop: marginTop,
        borderRadius: 7,
        
    }
}
export default BtnGradient;