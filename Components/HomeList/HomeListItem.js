import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
const HomeListItem = (props) =>{
    <View style = {style.listItem}>
        <Text>{props.name}</Text>
    </View>
}
const styles = StyleSheet.create({
    listItem:{
        width: '90%',
        marginLeft: '5%',
        paddingRight: 10,
        backgroundColor: 'gray',
    }
});
export default HomeListItem;