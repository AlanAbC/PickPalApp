import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Image} from 'react-native';
class Input extends Component {
    state = {
        width: this.props.width, 
        height: this.props.height,
    }
    render = () => {
        return(
            <View style={ViewInputStyle(this.props.width, this.props.height, this.props.marginTop)}>
                <TextInput
                style={InputStyle.input}
                placeholder={this.props.placeholder}
                secureTextEntry={this.props.secureTextEntry}
                onChangeText={this.props.onChangeText}
                value={this.props.value}
                autoCapitalize={this.props.autoCapitalize}
                spellCheck={this.props.spellCheck}
                underlineColorAndroid= 'rgba(0,0,0,0)'
                keyboardType={this.props.keyboardType}
                editable={this.props.editable}
                onEndEditing = {this.props.onEndEditing}
                returnKeyType='done'
                />
            </View>
        );
    }
}
const InputStyle = StyleSheet.create({
    input: {
        paddingLeft: 12,
        width: '90%',
        height: '100%',
        color: '#5A6872',
    },
    icon:{
        width: '7%',
        marginLeft: '1.5%',
        backgroundColor: 'red'
    }

})
const ViewInputStyle = function (width, height, marginTop) {
    return {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#FFFFFF',
        borderColor: '#D7D7D7',
        borderWidth: 1,
        height: height,
        width: width,
        marginTop: marginTop,
    } 
}
export default Input;