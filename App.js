import {Navigation} from 'react-native-navigation';
import Registro from './Views/Registro';
import Login from './Views/Login';
import RegisterPhone from './Views/RegisterPhone';
import ValidatePhoneCode from './Views/ValidatePhoneCode';
import Home from './Views/Home';
import FirstScreen from './Views/FirstScreen';
import Sliders from './Views/Sliders';
import CompleteRegister from './Views/CompleteRegister'

//Register Screens
Navigation.registerComponent("Pickpal.Registro", ()=> Registro);
Navigation.registerComponent("Pickpal.Login", ()=> Login);
Navigation.registerComponent("Pickpal.RegisterPhone", ()=> RegisterPhone);
Navigation.registerComponent("Pickpal.ValidatePhoneCode", ()=> ValidatePhoneCode);
Navigation.registerComponent("Pickpal.Home", ()=> Home);
Navigation.registerComponent("Pickpal.FirstScreen", ()=> FirstScreen);
Navigation.registerComponent("Pickpal.Sliders", ()=> Sliders);
Navigation.registerComponent("Pickpal.CompleteRegister", ()=> CompleteRegister);

//Start App
Navigation.startSingleScreenApp({
  screen:{
    screen: "Pickpal.Sliders",
    title: "Sliders",
    navigatorStyle: {
      navBarHidden: true
    }
  }
});