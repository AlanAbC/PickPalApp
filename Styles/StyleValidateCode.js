'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topscreen: {
        width: '100%',
		height: '50%',
		alignItems: 'center',
    },
    bottomscreen : {
        flexDirection: 'column',
        width: '100%',
        height: '40%',
		alignItems: 'center',
	},
	loginIntTxt: {
        color: '#5A6872',
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '13%',
        width: '70%'
    },
    loginIntTxt2: {
        color: '#5A6872',
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    loginDescrp2: {
        color: '#5A6872',
        fontSize: 14,
        fontWeight: 'normal',
        textAlign: 'center',
        marginTop: 50,
        width: '90%'
	},
	loginDescrp: {
        color: '#5A6872',
        fontSize: 14,
        fontWeight: 'normal',
        textAlign: 'center',
        marginTop: 10,
        width: '90%'
	},
    input: {
        width: '70%',
        marginTop: 20
    },
    button:{
        color: '#000000'

    },
    bottomText:{
        marginTop: 30,
        color: '#5A6872',
    },
    bottomcontainer: {
        flexDirection: 'column',
        width: '100%',
        height: '10%',
		alignItems: 'center',
    }, 
    codeText: {

    },
})

module.exports = myStyles;
