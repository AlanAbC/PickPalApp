'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
	container: {
        flexDirection: 'column',
        backgroundColor: '#DEDCDE',
        alignItems: 'center',
        justifyContent: 'center'
    },
   lista: {
		width: '100%',
		height: '90%',
		backgroundColor: 'orange'
   },
   card: {
	
   },
   txtTitleCard: {
	   width : '100%',
	   marginTop: 5,
	   paddingLeft: '3%',
	   color: '#F54840',
	   fontSize: 20,
   },
   txtPhoneCard: {
	width : '100%',
	marginTop: 7,
	paddingLeft: '3%',
	color: '#868686',
	fontSize: 16,
   }
})

module.exports = myStyles;