'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        width: '70%',
        borderWidth: 1,
        borderColor: '#D9D9D9',
        backgroundColor: '#F2F2F2',
        marginTop: 20
    },
    button:{
        color: '#000000'

    }
})

module.exports = myStyles;