'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnfacebook:{
        width: '90%',
        height: 50,
        backgroundColor: '#3B5998',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 7,
        marginTop: '10%'
    },
    mailButtonStyle:{
        width: '33.3333%',
        backgroundColor: 'white',
        alignItems: 'center',
    },
    instagramButton:{
        width: '90%',
        height: 50,
        backgroundColor: '#FC664F',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 7,
        marginTop: '6%'
    },
    topscreen: {
        width: '100%',
        height: '33%',
    },
    bottomscreen : {
        height: '67%',
        width: '100%',
        alignItems: 'center',
    },
    titletopscreen:{
        width: '100%',
        color: '#ffffff',
        fontSize: 25,
        marginTop: 80,
        textAlign: 'center',
    },
    input: {
        width: '70%',
        borderWidth: 1,
        borderColor: '#868686',
        backgroundColor: '#d9d9d9',
        borderRadius: 10,
        marginTop: 50
    },
    button:{
        color: '#000000'

    },
    contentContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: 375,
        height: 300,
      },
    textContainer: {
        color: 'white',
        fontSize: 22,
    },
    imgIcon:{
        height: 35,
        width: 35,
        marginLeft: 15,
    },
    imgIconIns: {
        height: 30,
        width: 30,
        marginLeft: 15,
    },
    txtbtnIcon: {
        color:'#ffffff',
        fontSize: 20,
        marginLeft: 10
    },
    txtdescuida: {
        width: '85%',
        marginTop: '5%',
        color: '#5A6872',
        fontSize: 14,
        textAlign: 'center'
    },
    txtregistro: {
        width: '85%',
        marginTop: '10%',
        color: '#5A6872',
        fontSize: 14,
        textAlign: 'center'
    },
    loginbtn: {
        width: '50%',
        marginTop: '5%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    txtLogin: {
        width: '85%',
        color: '#5A6872',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    txtTerms: {
        width: '80%',
        textAlign: 'center',
        fontSize: 14,
        marginTop: '7%',
        color: '#8E9AA3'
    }
})

module.exports = myStyles;
