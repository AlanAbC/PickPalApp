'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    topscreen: {
        width: '100%',
        height: '70%',
        backgroundColor: 'transparent'
    },
    bottomscreen : {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        height: '30%',
        alignItems: 'center',
    },
    loginbtn: {
        width: '50%',
        marginTop: '5%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    txtLogin: {
        width: '85%',
        color: '#5A6872',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    txtTerms: {
        width: '80%',
        textAlign: 'center',
        fontSize: 14,
        marginTop: '7%',
        color: '#8E9AA3'
    },
    txtdescuida: {
        width: '85%',
        marginTop: '5%',
        color: '#5A6872',
        fontSize: 14,
        textAlign: 'center'
    },
})

module.exports = myStyles;
