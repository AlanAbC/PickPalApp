'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
   container: { 
   		flex: 1,
	    flexDirection: 'column',
	    backgroundColor: '#FFFFFF',
	    alignItems: 'center',
   },
   txtTitle: {
        width: '75%',
        color: '#5A6872',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 20
    },
    txtSubTitle: {
        width: '90%',
        color: '#5A6872',
        fontSize: 15,
        textAlign: 'center',
        marginTop: 5
    },
    radioContainer: {
        marginTop: 10,
        width: '80%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#FFFFFF',
        borderColor: '#D7D7D7',
        borderWidth: 1,
    }
})

module.exports = myStyles;