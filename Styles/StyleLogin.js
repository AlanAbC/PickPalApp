'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
   container: { 
   		flex: 1,
	    flexDirection: 'column',
	    backgroundColor: '#FFFFFF',
	    alignItems: 'center',
	},
	topscreen: {
		width: '100%',
		height: '20%',
		alignItems: 'center',
    },
    bottomscreen : {
        flexDirection: 'column',
        width: '100%',
        height: '65%',
		alignItems: 'center',
	},
	loginIntTxt: {
        color: '#5A6872',
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
		marginTop: '30%',
	},
	loginDescrp: {
        color: '#5A6872',
        fontSize: 15,
        fontWeight: 'normal',
        textAlign: 'center',
		marginTop: 10,
		width: '90%'
	},
	loginDescrp2: {
        color: '#5A6872',
        fontSize: 16,
        fontWeight: 'normal',
        textAlign: 'center',
	},
	backUpPass: {
        color: '#5A6872',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
	},
   textLogin:{
	   fontSize: 29,
	   color: 'orange',
	   textAlign: 'center',
	   width: '100%'
   },
   inputTextStyle: {
	   marginTop: '3%',
   },
   inputView: {
	   marginTop: '10%'
   },
   style2: { 
   		flex: 1,
		width: '85%',
		marginLeft: '7.5%',
		borderRadius: 5,
		backgroundColor: '#00000000',
		height: 100,
		alignItems: 'center',
		justifyContent: 'center'
   },
   
})

module.exports = myStyles;