'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        width: '70%',
        marginTop: 20
    },
    button:{
        color: '#000000'

    },
    bottomText1:{
        marginTop: 20
    },
    bottomText:{
        marginBottom: 30
    },
    txtTitle: {
        width: '95%',
        color: '#5A6872',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '15%',
    },
    txtSubTitle: {
        width: '80%',
        color: '#5A6872',
        fontSize: 12,
        textAlign: 'center',
        marginTop: 5
    },
})

module.exports = myStyles;
3