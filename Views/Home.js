import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator, Alert } from "react-native";
import { List, ListItem, SearchBar, Card } from "react-native-elements";
import ip from '../Config/AppConfig';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      schedules: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = ip + 'establishment/view_all_establishment/';
    this.setState({ loading: true });

    fetch(url)
      .then(res => {
        if(res.status == 200){
            data = JSON.parse(res._bodyText);
            console.log(data);
            this.setState({
                data: data,
                error: res.error || null,
                loading: false,
                refreshing: false
              });
        }else{
            Alert.alert(
                'Error',
                'Servidor no disponible',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
      })
      .catch(error => {
        this.setState({showTheThing: false}) 
            Alert.alert(
              'Error',
              'Servidor no disponible',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
      });
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };


  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    var s = require('../Styles/StyleHome');
    return (
      <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0, backgroundColor: '#f2f2f2' }}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <Card
            style = {s.card}
            image={{uri: item.logo}}>
            <Text style = {s.txtTitleCard}>{item.establishment_name}</Text>
            <Text style = {s.txtPhoneCard}>Telefono: {item.phone}</Text>
            </Card>
          )}
          keyExtractor = {item => item.id}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
        />
      </List>
    );
  }
}

export default Home;
