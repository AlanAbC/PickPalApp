import {Navigation} from 'react-native-navigation';
import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
import ApiUtils from '../Config/ApiUtils';
import Button from 'react-native-button';
import LocalStorage from '../Config/LocalStorage';
import ip from '../Config/AppConfig';
import Input from '../Components/Input/Input';
import BtnGradient from '../Components/BtnGradient/BtnGradient';
class Registro extends Component{
    state = {
        firstName: '',
        lastName: '',
        mail: '',
        password: '',
        passconfirm: '',
        username: '',
        phone: ''
    };
    _register = (mail, password, passconfirm, phone) =>{
        debugger;
        this.setState({showTheThing: true}) 
        if(mail != '' && password != '' && passconfirm != '' && phone != ''){
            if(password === passconfirm){
                fetch(ip + 'users/register/',{
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        email: mail,
                        password: password,
                        phone: phone,
                      }),
                })
                .then((response) => {
                    debugger;
                    if(response.status == 200){
                        this.setState({showTheThing: false}) 
                        data = JSON.parse(response._bodyText);
                        this.props.navigator.push({
                            screen: 'Pickpal.ValidatePhoneCode', 
                            title: 'Validar Telefono',
                            passProps: {client_id: data.client_id, register: true},
                            navigatorStyle: {
                                navBarHidden: true
                              }
                        });
                    }else if(response.status == 302){
                        this.setState({showTheThing: false}) 
                        Alert.alert(
                            'Error',
                            'Correo ya registrado',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    }else{
                        this.setState({showTheThing: false}) 
                        Alert.alert(
                            'Error',
                            'Servidor no disponible',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    }
                })
                .catch((error) => {
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                    'Error',
                    'Servidor no disponible',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                    )
                    return;
                });
            }else{
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'Las contraseñas no coinciden',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
            
        }else{
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'Por favor ingresa todo los campos',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
    };
    mailChangeHandler = val =>{
        this.setState({
            mail: val
        });
    };
    passwordChangeHandler = val =>{
        this.setState({
            password: val
        });
    };
    passConfChangeHandler = val =>{
        this.setState({
            passconfirm: val
        });
    };
    phoneChangeHandler = val =>{
        this.setState({
            phone: val
        });
    };
    render(){
        var s = require('../Styles/StyleRegistro');
        return (
            <ScrollView contentContainerStyle={s.container} keyboardShouldPersistTaps = {'handled'}>
                <Text style = {s.txtTitle}>Para registrarte con tu correo electrónico {"\n"}llena los sisguientes campos</Text>
                <Text style ={s.txtSubTitle}>(Enivaremos un código a tu celular via SMS para verificar tu teléfono)</Text>
                <Input autoCapitalize = 'none' width = {'80%'} height = {50} keyboardType = 'email-address' marginTop = {25} placeholder={'Correo'} onChangeText={this.mailChangeHandler}/>
                <Input width = {'80%'} height = {50} keyboardType = 'numeric' marginTop = {20} placeholder={'Telefono'} onChangeText={this.phoneChangeHandler}/>
                <Input secureTextEntry={true} width = {'80%'} height = {50} marginTop = {20} placeholder={'Contraseña'} onChangeText={this.passwordChangeHandler}/>
                <Input secureTextEntry={true} width = {'80%'} height = {50} marginTop = {20} placeholder={'Confirmar Contraseña'} onChangeText={this.passConfChangeHandler}/>
                <BtnGradient onPress= {() => this._register(this.state.mail, this.state.password, this.state.passconfirm, this.state.phone)} text = {'Continuar'} width = {'80%'} marginTop = {'15%'}/>
                
            </ScrollView>
        );
    }
}
export default Registro;