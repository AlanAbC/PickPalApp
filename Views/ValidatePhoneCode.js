import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, ActivityIndicator, TouchableOpacity, StatusBar} from 'react-native';
import Button from 'react-native-button';
import ApiUtils from '../Config/ApiUtils';
import ip from '../Config/AppConfig';
import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import Input from '../Components/Input/Input';
import ButtonGradient from '../Components/BtnGradient/BtnGradient';
class ValidatePhoneCode extends Component{
    static navigatorStyle = {
        navBarTextColor: 'white',
        navBarBackgroundColor: 'white', 
        navBarButtonColor: 'black',
        navBarNoBorder: true,
      }
    state = {
        code: '',
        client_id: this.props.client_id,
    };
    codeChangeHandler = val =>{
        this.setState({
            code: val
        });
    };
    _onPressSendCode = () =>{
        this.setState({showTheThing: true}) 
        fetch(ip + 'users/resend_code/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                client_id: this.state.client_id,
              }),
        })
        .then((response) => {
            if(response.status == 200){
                this.setState({showTheThing: false}) 
            }else{
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'Servidor no disponible',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
            this.setState({showTheThing: false}) 
                    Alert.alert(
                    'Error',
                    'Servidor no disponible',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                    )
                    return;
            });
    }
    _validate = (code, client_id) =>{
        debugger;
        this.setState({showTheThing: true}) 
        if(code != ''){
            fetch(ip + 'users/validate_phone_code/',{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    client_id: client_id,
                    code: code,
                  }),
            })
            .then((response) => {
                if(response.status == 200){
                    this.setState({showTheThing: false}) 
                    data = JSON.parse(response._bodyText);
                    console.log(data);
                    ls.save('email', data.email)
                    ls.save('id', data.id)
                    ls.save('avatar', data.avatar)
                    if(this.props.register){
                        this.props.navigator.push({
                            screen: 'Pickpal.CompleteRegister', 
                            title: 'Home',
                            navigatorStyle: {
                                navBarHidden: true
                              },
                             passProps: {client_id: data.id}
                        });
                    }else{
                        this.props.navigator.resetTo({
                            screen: 'Pickpal.Home', 
                            title: 'Home',
                        });
                    }
                    
                }else if(response.status == 404){
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'Codigo Incorrecto',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }else{
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'Servidor no disponible',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }
            })
            .catch((error) => {
                this.setState({showTheThing: false}) 
                    Alert.alert(
                    'Error',
                    'Servidor no disponible',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                    )
                    return;
                });
        }else{
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'Por favor ingresa un codigo',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
    };
    render(){
        var s = require('../Styles/StyleValidateCode');
        return(
            <View style={s.container}>
                <StatusBar hidden={false}/>
                    <Text style ={s.loginIntTxt}>Te hemos enviado un Código vía SMS</Text>
                    <Text style={s.loginDescrp}>Por favor digita el código de 5 dígitos en los siguientes campos</Text>
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="orange" />
                }
                
                <Input
                width = {'80%'} height = {50} marginTop = {40}
                placeholder="Codigo"
                keyboardType='numeric'
                onChangeText={this.codeChangeHandler}
                value={this.state.code}
                />
                <TouchableOpacity onPress={this._onPressSendCode}>
                    <Text style= {s.bottomText}>Reenviar Codigo</Text>
                </TouchableOpacity>
                <ButtonGradient text = {'Verificar'} width = {'80%'} marginTop = {30} onPress={() => this._validate(this.state.code, this.state.client_id)}/>
                
            </View>
        );
    }
}
export default ValidatePhoneCode;