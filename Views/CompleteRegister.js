import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator } from 'react-native';
import Button from 'react-native-button';
import ls from 'react-native-local-storage';
import ip from '../Config/AppConfig';
import BtnGradient from '../Components/BtnGradient/BtnGradient';
import Input from '../Components/Input/Input';
import DatePicker from 'react-native-datepicker';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: 'Masculino', value: 'M' },
  {label: 'Femenino', value: 'F' }
];
export default class CompleteRegister extends React.Component {
  state = {
    emailPlaceHolder: 'Correo Electronico',
    namePlaceHolader: 'Nombre',
    dateFlaceHolder: 'Fecha de Nacimiento',
    zipPlaceHolder: 'Codigo Postal',
    genderPlaceHolder: 'Sexo',
    editableEmail: true,
    email: '',
    name: '',
    date: '',
    zip: '',
    gender: 'M'
  };
  componentDidMount(){
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const url = ip + 'users/view_profile/'+ this.props.client_id + '/';
    this.setState({ loading: true });

    fetch(url)
      .then(res => {
        if(res.status == 200){
            data = JSON.parse(res._bodyText);
            console.log(data);
            if(data.full_name != ""){
              this.setState({
                namePlaceHolader: data.full_name,
                name: data.full_name,
              });
            }
            if(data.email != ""){

              this.setState({
                emailPlaceHolder: data.email,
                email: data.email,
                editableEmail: false
              });
            }
            if(data.birth_date != ""){
              this.setState({
                dateFlaceHolder: data.birth_date,
                date: data.birth_date
              });
            }
            if(data.zip_code != ""){
              this.setState({
                zipPlaceHolder: data.zip_code,
                zip: data.zip_code,
              });
            }
            if(data.gender != ""){
              this.setState({
                genderPlaceHolder: data.gender,
                gender: data.gender,
              });
            }
        }else{
            Alert.alert(
                'Error',
                'Servidor no disponible',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
      })
      .catch(error => {
        this.setState({showTheThing: false}) 
            Alert.alert(
              'Error',
              'Servidor no disponible',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
      });
  };
  register = (email, name, gender, birth_date, zip_code) =>{
    this.setState({showTheThing: true}) 
    if(email != '' && name != '' && gender != '' && birth_date != '' && zip_code != ''){
      string = (name).split(" ");
           fname =  string[0];
           lname = ""
           if(string.length > 1){
            for(i = 1; i<string.length; i++){
                  lname+= " " + string[i]
            }
           }
        fetch(ip + 'users/update_profile/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                client_id: this.props.client_id,
                email: email,
                first_name:fname,
                last_name:lname,
                gender: gender,
                birth_date: birth_date,
                zip_code: zip_code,
                is_register: 1
              }),
        })
        .then((response) => {
          console.log(response)
            if(response.status == 200){
                this.setState({showTheThing: false}) 
                this.props.navigator.resetTo({
                    screen: 'Pickpal.Home', 
                    title: 'Home',
                });
            }else if(response.status == 404){
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'El usuario no existe',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else if (response.status == 401){
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'El usuario no está activo',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'Servidor no disponible',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
            this.setState({showTheThing: false}) 
            Alert.alert(
              'Error',
              'Servidor no disponible',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
        });
    }else{
      this.setState({showTheThing: false}) 
        Alert.alert(
            'Error',
            'Por favor ingresa todo los campos',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }
    };
  emailChangeHandler = val =>{
      this.setState({
          email: val
      });
  };
  nameChangeHandler = val =>{
      this.setState({
          name: val
      });
  };
  genderChangeHandler = val =>{
    this.setState({
        gender: val
    });
  };
  dateChangeHandler = val =>{
      this.setState({
          date: val
      });
  };
  zipChangeHandler = val =>{
    this.setState({
        zip: val
    });
  };
  checkemail(){
      fetch(ip + 'users/validate_email/',{
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: this.state.email,
          }),
    })
    .then((response) => {
      console.log(response)
        if(response.status == 200){
          
        }else if(response.status == 302){
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'El correo ya  esta ocupado',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }else{
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'Servidor no disponible',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
    })
    .catch((error) => {
        this.setState({showTheThing: false}) 
        Alert.alert(
          'Error',
          'Servidor no disponible',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
        return;
    });
  }
  render() {
    
    var s = require('../Styles/StyleCompleteRegister');
    return (
      <View style={s.container}>
            <Text style = {s.txtTitle}>¡Casi listos! Por favor {"\n"}completa tu perfil</Text>
            <Text style ={s.txtSubTitle}>Con estos datos podras tener una mejor experiencia dentro de nuestra plataforma</Text>
            <Input width = {'80%'} height = {50} marginTop = {20} placeholder={this.state.namePlaceHolader} onChangeText={this.nameChangeHandler}/>
            <Input onEndEditing={() => this.checkemail()}  editable={this.state.editableEmail} width = {'80%'} height = {50} marginTop = {10} placeholder={this.state.emailPlaceHolder} onChangeText={this.emailChangeHandler}/>
            <View style ={s.radioContainer}>
              <RadioForm
                radio_props={radio_props}
                formHorizontal={true}
                buttonColor={'#FC664F'}
                onPress={(value) => {this.setState({gender:value})}}
              />  
            </View>  
            <DatePicker
            style={{width: '80%', marginTop: 10}}
            date={this.state.date}
            mode="date"
            placeholder={this.state.dateFlaceHolder}
            format="YYYY-MM-DD"
            minDate="1918-05-01"
            maxDate="2010-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateInput: {
                width: '100%',
              }
            }}
            onDateChange={(date) => {this.setState({date: date})}}
          />
            <Input keyboardType = {'numeric'} width = {'80%'} height = {50} marginTop = {10} placeholder={this.state.zipPlaceHolder} onChangeText={this.zipChangeHandler}/>
            <BtnGradient onPress= {() => this.register(this.state.email, this.state.name, this.state.gender, this.state.date, this.state.zip)} text = {'Finalizar Registro'} width = {'80%'} marginTop = {'15%'}/>
      </View>
    );
  }
}