import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, TouchableOpacity, StatusBar} from 'react-native';
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';
import Button from '../Components/BtnGradient/BtnGradient';

const datacarousel = [
    {
        "id": 339964,
        "imagePath": require('../assets/Images/Slide1.png'),
    },
    {
        "id": 315635,
        "imagePath": require('../assets/Images/Slide2.png'),
    },
    {
        "id": 339403,
        "imagePath": require('../assets/Images/Slide3.png'),
    },
]

class FirstScreen extends Component{
    _onPressLogin = () => {
        this.props.navigator.push({
            screen: 'Pickpal.Login', 
            title: 'Login',
            navigatorStyle: {
                navBarHidden: true
              }
        });
    }
    _onPressRegister = () => {
        this.props.navigator.push({
            screen: 'Pickpal.FirstScreen', 
            title: 'First Screen',
            navigatorStyle: {
                navBarHidden: true
              }
        });
    }
    render(){
        var _this = this;
        var s = require('../Styles/StylesSlider');
        return(
            <View style={s.container}>
                <StatusBar hidden={true}/>
                <View style = {s.topscreen}>
                    <SwipeableParallaxCarousel data={datacarousel} parallax={false} height='100%' navigationType='dots' navigationColor='#5A6872' navigation={true}/>
                </View>
                <View style = {s.bottomscreen}>
                    <Button text = {'Regístrate en Pick Pal'} width = {'80%'} marginTop = {15} onPress={() => this._onPressRegister()}/>
                    <TouchableOpacity onPress={() => this._onPressLogin()} style = {s.loginbtn}>
                        <Text style = {s.txtLogin}>¿Ya tienes cuenta?</Text>
                        <Text style = {s.txtdescuida}>Inicia sesion</Text>
                    </TouchableOpacity>
                    <TouchableOpacity ><Text style ={s.txtTerms}>Terminos y condiciones</Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}
export default FirstScreen;