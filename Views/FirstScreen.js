import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, TouchableOpacity, Image, ImageBackground} from 'react-native';
import Button from 'react-native-button';
import PropTypes from 'prop-types';
import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import ApiUtils from '../Config/ApiUtils';
import ip from '../Config/AppConfig';
import ls from 'react-native-local-storage';
import Carousel from 'react-native-carousel-view';
import InstagramLogin from 'react-native-instagram-login';

class FirstScreen extends Component{
    
    _loginFacebook = (_this) => {
        FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.Native); 

        FBLoginManager.loginWithPermissions(["email"], function(error, data){
          if (!error) {
            console.log("Login data: ", data);
            if (data.profile) {
                profile = JSON.parse(data.profile);
                fetch(ip + 'login/',{
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        token: data.credentials.token,
                        id: data.credentials.userId,
                        email: profile.email,
                        first_name: profile.first_name,
                        last_name: profile.last_name,
                        record_medium: "2"
                      }),
                })
                .then((response) => {
                    console.log(response);

                    if(response.status == 200){
                        data = JSON.parse(response._bodyText);
                        _this.props.navigator.push({
                            screen: 'Pickpal.RegisterPhone', 
                            title: 'Validar Telefono',
                            passProps: {client_id: data.client_id, is_register: data.is_register},
                            navigatorStyle: {
                                navBarHidden: true
                              }
                        });
                    }else{
                        Alert.alert(
                            'Error',
                            'Servidor no disponible',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    }
                })
                .catch((error) => {
                    Alert.alert(
                        'Error',
                        'Servidor no disponible',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                });
            }else{
                Alert.alert(
                    'Error',
                    'Imposible logearse con facebook',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
          } else {
            Alert.alert(
                'Error',
                'Imposible logearse con facebook',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
          }
        })
    }
    _loginWithInstagram(access_token, _this) {
        fetch('https://api.instagram.com/v1/users/self/?access_token='+access_token)
       .then((response) => response.json()).then((responseData) => {
           console.log(responseData);
           string = (responseData.data.full_name).split(" ");
           fname =  string[0];
           lname = ""
           if(string.length > 1){
            for(i = 1; i<string.length; i++){
                  lname+= " " + string[i]
            }
           }
           if (responseData.meta.code === 200){
            fetch(ip + 'login/',{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    token: access_token,
                    id: responseData.data.id,
                    first_name: fname,
                    last_name: lname,
                    record_medium: "3"
                  }),
            })
            .then((response) => {
                debugger;
                console.log(response);
                if(response.status == 200){
                    data = JSON.parse(response._bodyText);
                    this.props.navigator.push({
                        screen: 'Pickpal.RegisterPhone', 
                        title: 'Validar Telefono',
                        passProps: {client_id: data.client_id, is_register: data.is_register},
                        navigatorStyle: {
                            navBarHidden: true
                          }
                    });
                }else{
                    Alert.alert(
                        'Error',
                        'Servidor no disponible',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }
            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    'Servidor no disponible',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            });
           }else{
            Alert.alert(
                'Error',
                'Imposible logearse con Instagram',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
           }
       })
    }
    _setLogin = () => {
        this.props.navigator.push({
            screen: 'Pickpal.Registro', 
            title: 'Registro',
            navigatorStyle: {
                navBarHidden: true
              }
        });
      };
    _onPressLogin = () => {
    this.props.navigator.push({
        screen: 'Pickpal.Login', 
        title: 'Login',
        navigatorStyle: {
            navBarHidden: true
          }
    });
    }
      

    render(){
        var _this = this;
        var s = require('../Styles/StyleFirstScreen');
        return(
            <View style={s.container}>
                <Image source = {require('../Img/header.png')} resizeMode= {'cover'} style = {s.topscreen}/>
                <View style = {s.bottomscreen}>
                    <TouchableOpacity onPress={() => this._loginFacebook(_this)} style={s.btnfacebook}>
                        <Image resizeMode={'center'} style = {s.imgIcon} source={require('../Img/facebook.png')}/>
                        <Text style ={s.txtbtnIcon}>Registrate con Facebook</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.refs.InstagramLogin.show()} style={s.instagramButton}>
                    <ImageBackground source = {require('../Img/gradient.png')} resizeMode= {'cover'} style = {{width: '100%', height: '100%', flexDirection: 'row', alignItems: 'center',}} resizeMethod={'resize'} >
                        <Image resizeMode={'center'} style = {s.imgIconIns} source={require('../Img/instagram.png')}/>
                        <Text style ={s.txtbtnIcon}>Registrate con Instagram</Text>
                        <InstagramLogin
                            ref='InstagramLogin'
                            clientId='293cf5234a4c4e9a904680ce12aeda75'
                            redirectUrl='http://tickets.iwsandbox.com:9000/'
                            scopes={['public_content', 'follower_list']}
                            onLoginSuccess={(token) => this._loginWithInstagram(token, this)}
                            onLoginFailure={(data) => console.log(data)}
                        />
                    </ImageBackground>    
                    </TouchableOpacity>
                    <Text style = {s.txtdescuida}>Descuida, no publicamos nada en tus redes sociales sin tu permiso</Text>
                    <TouchableOpacity onPress={() => this._setLogin()}><Text style ={s.txtregistro}>Registro via correo electronico</Text></TouchableOpacity>
                    <TouchableOpacity onPress={() => this._onPressLogin()} style = {s.loginbtn}>
                        <Text style = {s.txtLogin}>¿Ya tienes cuenta?</Text>
                        <Text style = {s.txtdescuida}>Inicia sesion</Text>
                    </TouchableOpacity>
                    <TouchableOpacity ><Text style ={s.txtTerms}>Terminos y condiciones</Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}
export default FirstScreen;