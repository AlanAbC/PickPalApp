import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, StatusBar } from 'react-native';
import Button from 'react-native-button';
import ls from 'react-native-local-storage';
import ip from '../Config/AppConfig';
import ButtonGradient from '../Components/BtnGradient/BtnGradient';
import {Navigation} from 'react-native-navigation';
import Input from '../Components/Input/Input';

export default class App extends React.Component {
  static navigatorStyle = {
    navBarTextColor: 'white',
    navBarBackgroundColor: 'white', 
    navBarButtonColor: 'black',
    navBarNoBorder: true,
  }
  state = {
    email: '',
    password: ''
  };
  _login = (email, password) =>{
    this.setState({showTheThing: true}) 
    if(email != '' && password != ''){
        fetch(ip + 'login/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                email: email,
                password: password,
              }),
        })
        .then((response) => {
          console.log(response)
            if(response.status == 200){
                this.setState({showTheThing: false}) 
                data = JSON.parse(response._bodyText);
                this.props.navigator.push({
                    screen: 'Pickpal.ValidatePhoneCode', 
                    title: 'Registro',
                    passProps: {client_id: data.client_id},
                });
            }else if(response.status == 404){
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'El usuario no existe',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else if (response.status == 401){
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'El usuario no está activo',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'Servidor no disponible',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
            this.setState({showTheThing: false}) 
            Alert.alert(
              'Error',
              'Servidor no disponible',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
        });
    }else{
      this.setState({showTheThing: false}) 
        Alert.alert(
            'Error',
            'Por favor ingresa todo los campos',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }
    };
    emailChangeHandler = val =>{
        this.setState({
            email: val
        });
    };
    passwordChangeHandler = val =>{
        this.setState({
            password: val
        });
    };

  render() {
    
    var s = require('../Styles/StyleLogin');
    return (
      <View style={s.container}>
      <StatusBar hidden={false}/>
        <Text style ={s.loginIntTxt}>Inicia Sesión</Text>
        <Text style={s.loginDescrp}>Inicia Sesión a una cuenta existente con tu correo electrónico. Si no recuerdas tu contraseña haz tap en</Text>
        <TouchableOpacity>
          <Text style={s.backUpPass}>Recuperar Cuenta</Text>
        </TouchableOpacity>
        { this.state.showTheThing && 
          <ActivityIndicator size="large" color="orange" />
        }
        <View style={s.inputView}>
        <Input width = {'90%'} height = {50} marginTop = {10} placeholder={'Correo Electrónico'} onChangeText={this.emailChangeHandler} keyboardType={'email-address'}
          onChangeText={this.emailChangeHandler}
          value={this.state.email}
          autoCapitalize = 'none'
          />
          <Input width = {'90%'} height = {50} marginTop = {10} placeholder={'Contraseña'} onChangeText={this.passwordChangeHandler} secureTextEntry={true}
          onChangeText={this.passwordChangeHandler}
          value={this.state.password}
          autoCapitalize = 'none'
          />
          </View>
          <ButtonGradient
            text = {'Inicia Sesión'} width = {'80%'} marginTop = {30} onPress={() => this._login(this.state.email, this.state.password)}
          />
      </View>
    );
  }
}