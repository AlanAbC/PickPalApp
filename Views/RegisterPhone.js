import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, ActivityIndicator} from 'react-native';
import Button from 'react-native-button';
import ApiUtils from '../Config/ApiUtils';
import ip from '../Config/AppConfig';
import Input from '../Components/Input/Input';
import BtnGradient from '../Components/BtnGradient/BtnGradient';
class RegisterPhone extends Component{
    state = {
        phone: '',
        client_id: this.props.client_id,
    };
    phoneChangeHandler = val =>{
        this.setState({
            phone: val
        });
    };
    _register = (phone, client_id) =>{
        this.setState({showTheThing: true}) 
        if(phone != '' && client_id != ''){
            fetch(ip + 'users/validate_phone/',{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    phone: phone,
                    client_id: client_id,
                  }),
            })
            .then((response) => {
                console.log(response);
                if(response.status == 200){
                    this.setState({showTheThing: false}) 
                    if(this.props.is_register){
                        this.props.navigator.push({
                            screen: 'Pickpal.ValidatePhoneCode', 
                            title: 'Validar Telefono',
                            passProps: {client_id: this.state.client_id, register: true},
                            navigatorStyle: {
                                navBarHidden: true
                              }
                        });
                    }else{
                        this.props.navigator.push({
                            screen: 'Pickpal.ValidatePhoneCode', 
                            title: 'Validar Telefono',
                            passProps: {client_id: this.state.client_id, register: false},
                            navigatorStyle: {
                                navBarHidden: true
                              }
                        });
                    }
                    
                }else{
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'Servidor no disponible',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }
            })
            .catch((error) => {
                this.setState({showTheThing: false}) 
                Alert.alert(
                'Error',
                'Servidor no disponible',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
                )
                return;
                });
        }else{
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'Por favor ingresa todo los campos',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
    };
    render(){
        var s = require('../Styles/StyleRegisterPhone');
        return(
            <View style={s.container}>
                <Text style = {s.txtTitle}>Verifica tu Cuenta</Text>
                <Text style ={s.txtSubTitle}>Aqui es donde se redacta la razon por la cual estamos pidiendo el celular, y si es posible que beneficio ofrecemos por este datos personal </Text>
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="orange" />
                }
                <Input width = {'80%'} height = {50} keyboardType = 'numeric' marginTop = {20} placeholder={'Telefono'} onChangeText={this.phoneChangeHandler}/>
                <BtnGradient onPress= {() => this._register(this.state.phone, this.state.client_id)} text = {'Enviar Código SMS'} width = {'80%'} marginTop = {'20%'}/>
                
            </View>
        );
    }
}
export default RegisterPhone;